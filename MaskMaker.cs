using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace SpriteAnimation
{
    public static class MaskMaker
    {
        private static int count;
        public static Bitmap[] Create(Bitmap[] sprites, DirectoryInfo directory)
        {
            count = sprites.Count();
            Bitmap[] result = new Bitmap[count]; 
            if (!Exist(directory))
            {
                directory.Create();
                for(int i = 0; i < count; i++)
                {
                    result[i] = Transform(sprites[i]);
                    result[i].Save(directory + "\\Mask" + (i + 1).ToString() + ".png", ImageFormat.Png);
                }
            }    
            else
            {
                for (int i = 0; i < count; i++)
                    result[i] = new Bitmap(directory + "\\Mask" + (i + 1).ToString() + ".png");
            }
            return result;
        }

        private static bool Exist(DirectoryInfo directory)
        {
            if (directory.Exists)
            {
                var files = directory.GetFiles();
                if (files.Count() == count)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (!files.Contains(new FileInfo("Mask" + (i+1).ToString() + ".png")))
                            return false;
                    }
                    return true;
                }
                else return false;
            }
            else return false;
        }

        private static Bitmap Transform(Bitmap source)
        {
            Bitmap result = new Bitmap(source.Width, source.Height);
            for (int i = 0; i < source.Width; i++)
                for(int j = 0; j < source.Height; j++)
                {
                    if (source.GetPixel(i, j) == Color.FromArgb(0, 0, 0))
                        result.SetPixel(i, j, Color.FromArgb(255, 255, 255, 255));
                    else result.SetPixel(i, j, Color.FromArgb(255, 0, 0, 0));
                }
            return result;
        }
    }
}
