using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SpriteAnimation
{
    public class Character
    {
        public Character(int left, int top)
        {
            Left = left;
            Top = top;
        }

        public Bitmap[] Sprites;
        public Bitmap[] Masks;

        public Bitmap[] Sprites180;
        public Bitmap[] Masks180; 

        public Bitmap Background;
        public bool visionRight;

        public int Width { get; set; }
        public int Height { get; set; }

        private double currentLeft;
        private double currentTop;

        private int expectedLeft;
        private int expectedTop;

        private double dx;
        private double dy;

        private int state = 1;
        
        public int Left { get; private set; }
        public int Top { get; private set; }

        public void Request(int reqLeft, int reqTop)
        {
            if (reqLeft < 0)
                reqLeft = 0;
            if (reqLeft > 1280 - Width)
                reqLeft = 1280 - Width;
            if (reqTop < 0)
                reqTop = 0;
            if (reqTop > 720 - Height)
                reqTop = 720 - Height;
            expectedLeft = reqLeft;
            expectedTop = reqTop;

            currentLeft = Left;
            currentTop = Top;

            dx = expectedLeft - Left;
            dy = expectedTop - Top;

            if (Math.Abs(dx) >= Math.Abs(dy))
            {
                dy = dy / Math.Abs(dx) * 4;
                dx = dx / Math.Abs(dx) * 4;
            }
            else
            {
                dx = dx / Math.Abs(dy) * 4;
                dy = dy / Math.Abs(dy) * 4;
            }
            if (dx > 0)
                visionRight = true;
            if (dx < 0)
                visionRight = false;
        }

        public bool Move()
        {
            currentLeft += dx;
            currentTop += dy;
            Left = (int)Math.Round(currentLeft);
            Top = (int)Math.Round(currentTop);
            if (!(Left.CompareTo(expectedLeft) * dx < 0 || Top.CompareTo(expectedTop) * dy < 0))
            { 
                Left = expectedLeft;
                Top = expectedTop;
            }
            return  (Left.CompareTo(expectedLeft)*dx < 0 || Top.CompareTo(expectedTop)* dy < 0);
        }

        public Tuple<Bitmap, Bitmap> Next()
        {
            state++;
            if (state > 5)
                state = 0;
            if (visionRight)
                return new Tuple<Bitmap, Bitmap>(Sprites180[state], Masks180[state]);
            else
            return new Tuple<Bitmap, Bitmap>(Sprites[state], Masks[state]);            
        }

        public Tuple<Bitmap, Bitmap> SetDefault()
        {
            state = 1;
            if (visionRight)
                return new Tuple<Bitmap, Bitmap>(Sprites180[state], Masks180[state]);
            else
                return new Tuple<Bitmap, Bitmap>(Sprites[state], Masks[state]);
        }
    }
}
