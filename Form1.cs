using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpriteAnimation
{
    public partial class Form1 : Form
    {
        Bitmap Background;
        Bitmap Cut;
        Graphics gr;
        Character Zombie;

        public Form1()
        {
            Zombie = new Character(0, 400);
            Zombie.visionRight = true;
            Zombie.Width = 192;
            Zombie.Height = 256;
            InitializeComponent();
        }
        

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Cut != null)
                gr.DrawImage(Cut, Zombie.Left, Zombie.Top);
            if (Zombie.Move())
                Draw(Zombie.Next());
            else
            {
                Draw(Zombie.SetDefault());
                timer1.Enabled = false;
            }

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            gr = this.CreateGraphics();
            Background = new Bitmap("Background.png");
            gr.DrawImage(Background, 0, 0, 1280, 720);
            Draw(Zombie.SetDefault());
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            Zombie.Request(e.X - Zombie.Width/2 - 15, e.Y - Zombie.Height + 35);
            timer1.Enabled = true;
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory()+"\\Sprites");
            if (dir.Exists)
            {
                var files = dir.GetFiles();
                int pngCount = 0;
                foreach (var file in files)
                {
                    if (file.Extension == ".png")
                        pngCount++;
                }
                Zombie.Sprites = new Bitmap[pngCount];
                Zombie.Sprites180 = new Bitmap[pngCount];
                for (int i = 0; i < pngCount; i++)
                {
                    try
                    {
                        Zombie.Sprites[i] = new Bitmap(dir + "\\Sprite" + (i + 1).ToString() + ".png");
                        Zombie.Sprites180[i] = new Bitmap(Zombie.Sprites[i]);
                    }
                    catch
                    {
                        MessageBox.Show("Important files do not found");
                        Dispose();
                    }
                }
                try
                {
                    Zombie.Masks = MaskMaker.Create(Zombie.Sprites, new DirectoryInfo(Directory.GetCurrentDirectory() + "\\Masks"));
                    Zombie.Masks180 = new Bitmap[Zombie.Masks.Count()];
                    for (int i = 0; i < Zombie.Sprites.Count(); i++)
                    {
                        Zombie.Masks180[i] = new Bitmap(Zombie.Masks[i]);
                        Zombie.Masks180[i].RotateFlip(RotateFlipType.RotateNoneFlipX);
                        Zombie.Sprites180[i].RotateFlip(RotateFlipType.RotateNoneFlipX);
                    }
                }
                catch
                {
                    MessageBox.Show("Important files do not found");
                    Dispose();
                }
            }
            else
            {
                MessageBox.Show("Important files do not found");
                Dispose();
            }
        }

        public void Draw(Tuple<Bitmap, Bitmap> tuple)
        {
            Bitmap result = new Bitmap(Zombie.Width, Zombie.Height);
            for(int i = Zombie.Left; i  <Zombie.Left + Zombie.Width; i++)
                for(int j = Zombie.Top; j < Zombie.Top+Zombie.Height; j++)
                {
                    Color newColor;
                    var back = Background.GetPixel(i, j);
                    var spr = tuple.Item1.GetPixel(i - Zombie.Left, j - Zombie.Top);
                    var mask = tuple.Item2.GetPixel(i - Zombie.Left, j - Zombie.Top);
                    int r = (back.R & mask.R) ^ spr.R;
                    int g = (back.G & mask.G) ^ spr.G;
                    int b = (back.B & mask.B) ^ spr.B;
                    newColor = Color.FromArgb(r, g, b);
                    result.SetPixel(i - Zombie.Left, j - Zombie.Top, newColor);
                }
            gr.DrawImage(result, Zombie.Left, Zombie.Top);
        }
    }
}